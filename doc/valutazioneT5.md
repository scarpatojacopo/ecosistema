## GRUPPO 5 
Marolda - Partesotti - Simonini - Scarpato - Majid

"GEOMETRIC SAVANNAH"


# Valutazione Presentazione e Prodotto (4,5 su 5, Peer 4)

Il prodotto si distingue per una logica di intelligenza artificiale molto raffinata, 
una buona organizzazione del codice e un'interfaccia grafica essenziale ma funzionale. 


La presentazione è stata preparata con cura, ed è risultata efficace anche se forse un po' tecnica. 


# Valutazione ultima consegna (4,5 su 5)

L'UML molto ben fatto, mi pare il migliore visto finora
Il codice è ben organizzato, con qualche idea originale e interessante. 
Il documento di analisi è un po' light. 
Nessun Javadoc

L'uso di git evidenzia un uso non corretto del sistema. I commit sono radi, e Marolda non l'ha mai usato. Questo non permette bene di capire il contributo dei singoli. 

     3  Leonardo Partesotti
     1  Nicolo Simonini
     1  Partesotti
     1  Ziad Majid
     2  nicolosimonini
     1  scarpato jacopo
     2  simonininicolo
     1  “nicolosimonini”


#Valutazione complessiva



In generale, pare che il gruppo nel suo complesso abbia avuto una evoluzione positiva, arrivando a una buona intesa organizzativa interna. Le relazioni sono tutte accettabili (simonini la presuminamo buona per motivi di malattia), particolarmente buona quella di partesotti. 
		VOTO	PROCESSO	PRODOTTO	RELAZ+CONTRIBUTO
MAROLDA		8,5	7		8,5		8
SIMONINI	8	7		8,5		7
SCARPATO	7,5	7		8,5		7,5
PARTESOTTI	7,5	6		8,5		8,5
ZIAD		6,5	6		8,5		5,5




