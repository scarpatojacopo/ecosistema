package game;

import display.Gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferStrategy;

public class Game implements ActionListener {

    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;
    public static final double FPS = 30.0;

    private Gui gui = new Gui();

    //private Thread gameThread = new Thread(this);
    //private FrameTimer fpsTimer = new FrameTimer(1/FPS * 1000);
    private int dt_ms = (int)(1/FPS * 1000);
    private double dt_sec = dt_ms/1000.0;
    private javax.swing.Timer frameTimer = new Timer(dt_ms,this);

    private World world = World.getInstance();

    Game() {
        //gameThread.start();
        frameTimer.start();
    }

    @Override
//    public void run() {
    public void actionPerformed(ActionEvent e) {
        //while(true) {
//            if (fpsTimer.tick()) {
                update();
                render();
//            }
        //}
    }

    private void update() {
        world.update(dt_sec);
    }

    private void render() {
        renderGame();
    }

    private void renderGame() {
        BufferStrategy buffer = gui.getCanvas().getBufferStrategy();
        if (buffer == null) {
            gui.getCanvas().createBufferStrategy(3);
            return ;
        }
        Graphics2D g = (Graphics2D) buffer.getDrawGraphics();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.clearRect(0, 0, WIDTH, HEIGHT);
        g.setFont(new Font("Arial", Font.BOLD, 8));
        world.render(g);
        RegionManager.getInstance().render(g);
        buffer.show();
        g.dispose();
    }

    public static void main(String[] args){
        Game game = new Game();
    }

}
