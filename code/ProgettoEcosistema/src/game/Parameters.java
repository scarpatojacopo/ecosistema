package game;

import java.util.Random;

public class Parameters {


    //Game
    public static final int INIT_GAZELLE_NUM = 50;
    public static final int INIT_LION_NUM = 30;
    public static final int INIT_ELEPHANT_NUM = 15;
    //public static final int INIT_WATER_NUM = 2;
    public static final double COLLISION_RANGE = 8;

    public static final double MAX_VALUE = 100;
    //Sight
    public static final double INIT_SIGHT_ELEPHANT = 40;
    public static final double INIT_SIGHT_GAZELLE = 30;
    public static final double INIT_SIGHT_LION = 30;
    //Speed
    public static final double INIT_SPEED_ELEPHANT = 20;
    public static final double INIT_SPEED_GAZELLE = 76;
    public static final double INIT_SPEED_LION = 80;
    public static final double HUNGER_DECAY_EXP_FACTOR = 0.007;
    //Boost
    public static final double BOOST_FACTOR_LION = 4;
    public static final double BOOST_FACTOR_GAZELLE = 2;
    public static final double BOOST_FACTOR_ELEPHANT = 2;
    //Age
    public static final double MAX_AGE_ELEPHANT = 70;
    public static final double MAX_AGE_GAZELLE = 8;
    public static final double MAX_AGE_LION = 20;
    //Food
    public static final double FOOD_THRESHOLD = 50;
    public static final double FOOD_DROP_BOOST_FACTOR = 1.2;
    public static final double FOOD_SHORTAGE_TH_LION = 30;
    public static final double FOOD_EAT_VALUE_LION = 50;
    public static final double FOOD_DROP_SEX_LION = 10;
    public static final double FOOD_DROP_RATE_LION = 18;
    public static final double FOOD_DROP_RATE_GAZELLE = 20;
    public static final double FOOD_EAT_RATE_GAZELLE = 60;
    public static final double FOOD_SHORTAGE_TH_GAZELLE = 2;
    public static final double FOOD_SHORTAGE_EXP_GAZELLE = 2;
    public static final double FOOD_DROP_RATE_ELEPHANT = 20;
    public static final double FOOD_EAT_RATE_ELEPHANT = 50;
    public static final double FOOD_SHORTAGE_TH_ELEPHANT = 1;
    public static final double FOOD_SHORTAGE_EXP_ELEPHANT= 2;
    //Water
    public static final double WATER_THRESHOLD = 50;
    public static final double WATER_DROP_BOOST_FACTOR = 1.2;
    public static final double WATER_SHORTAGE_TH_LION = 70;
    public static final double WATER_DRINK_VALUE_LION = 100;
    public static final double WATER_DROP_SEX_LION = 10;
    public static final double WATER_DROP_RATE_LION = 18;
    public static final double WATER_DROP_RATE_GAZELLE = 20;
    public static final double WATER_DRINK_VALUE_GAZELLE = 100;
    public static final double WATER_SHORTAGE_TH_GAZELLE = 5;
    public static final double WATER_SHORTAGE_EXP_GAZELLE = 2;
    public static final double WATER_DROP_RATE_ELEPHANT = 20;
    public static final double WATER_DRINK_VALUE_ELEPHANT = 100;
    public static final double WATER_SHORTAGE_TH_ELEPHANT = 150;
    public static final double WATER_SHORTAGE_EXP_ELEPHANT= 2;
    //Sex
    public static final double SEX_THRESHOLD = 65;
    public static final double SEX_INCREASE_RATE_GAZELLE = 40;
    public static final double SEX_INCREASE_RATE_LION = 30;
    public static final double PREGNANT_EXP_DECAY_FACTOR_LION = 0;
    public static final double PREGNANT_PROBABILITY_GAZELLE = 0.9;
    public static final double PREGNANT_PROBABILITY_LION = 0.75;
    public static final double NEW_BORN_DELIVER_RANGE = 60;
    public static final double SEX_INCREASE_RATE_ELEPHANT = 30;
    public static final double PREGNANT_EXP_DECAY_FACTOR_ELEPHANT = 0;
    public static final double PREGNANT_PROBABILITY_ELEPHANT = 0.9;
    //Mutation
    public static final double MUTATION_TOLERANCE_ELEPHANT = 0.2;
    public static final double MUTATION_TOLERANCE_LION = 0.2;
    public static final double MUTATION_TOLERANCE_GAZELLE = 0.2;
    public static final double MAX_MUTATION_FACTOR = 1.3;

    public static double constrainParametersInRange(double value, double min, double max) {
        value = Math.min(value, max);
        value = Math.max(value, min);
        return value;
    }

    public static double getRandomizedParameter(double value, double tolerance) {
            double t = (new Random().nextDouble() - 0.5) * 2 * tolerance;
        return value * (1 + t);
    }
}
