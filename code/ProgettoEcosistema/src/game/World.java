package game;

import entities.*;


import java.awt.*;
import java.util.ArrayList;
import utilities.Point;

public class World {

    public static World world;

    private boolean debugRender = false;

    private ArrayList<Elephant> elephants = new ArrayList<>();
    private ArrayList<Gazelle> gazelles = new ArrayList<>();
    private ArrayList<Lion> lions = new ArrayList<>();
    private ArrayList<Water> waters = new ArrayList<>();

    private World() {
        for (int i = 0; i < Parameters.INIT_ELEPHANT_NUM; i++) {
            elephants.add(new Elephant());
        }
        for (int i = 0; i < Parameters.INIT_GAZELLE_NUM; i++) {
            gazelles.add(new Gazelle());
        }
        for (int i = 0; i < Parameters.INIT_LION_NUM; i++) {
            lions.add(new Lion());
        }
        waters.add(new Water(new Point(190,420), 150));
        waters.add(new Water(new Point(700,200), 150));
        waters.add(new Water(new Point(250,170), 150));
        waters.add(new Water(new Point(500,500), 150));
    }

    /**
     *
     * @param dt tempo trascorso in secondi
     */
    public void update(double dt) {
        for (Elephant e : elephants)
            e.update(dt);
        for (Gazelle g : gazelles)
            g.update(dt);
        for (Lion l : lions)
            l.update(dt);

        //Remove dead and add new born:
        //@TODO anche removeList per ogni animale: il removeEntity con tutti gli instanceof è evidentemente over-engineering
        ArrayList<Elephant> newElephantList = new ArrayList<>();
        ArrayList<Gazelle> newGazelleList = new ArrayList<>();
        ArrayList<Lion> newLionList = new ArrayList<>();
        ArrayList<Entity> removeList = new ArrayList<>();

        for (Elephant e1 : elephants) {
            if (e1.isPregnant())
                newElephantList.add(e1.deliveryBaby());
            if (e1.getState() == Animal.State.DEAD)
                removeList.add(e1);
        }
        for (Gazelle g1 : gazelles) {
            if (g1.isPregnant())
                newGazelleList.add(g1.deliveryBaby());
            if (g1.getState() == Animal.State.DEAD)
                removeList.add(g1);
        }
        for (Lion l1 : lions) {
            if (l1.isPregnant())
                newLionList.add(l1.deliveryBaby());
            if (l1.getState() == Animal.State.DEAD)
                removeList.add(l1);
        }
            for(Entity e: removeList)
                removeEntity(e);
            elephants.addAll(newElephantList);
            gazelles.addAll(newGazelleList);
            lions.addAll(newLionList);
    }

    public void render (Graphics g){
        for (Elephant e1 : elephants)
            e1.render(g);
        for (Gazelle g1 : gazelles)
            g1.render(g);
        for (Lion l1 : lions) {
            l1.render(g);
        }
        for (Water w1 : waters) {
            w1.render(g);
        }

        if (debugRender) {
            g.setColor(new Color(0xF8889F));
            g.setFont(new Font("Arial", Font.BOLD, 10));
        }
    }

    private void removeEntity (Entity e){
        if (e instanceof Elephant)
            elephants.remove(e);
        else if (e instanceof Gazelle)
            gazelles.remove(e);
        else if (e instanceof Lion)
            lions.remove(e);
        RegionManager.getInstance().unregister(e);
    }

    public Water getNearestWater(Point p){
        Water foundW=null;
        double foundDist=0;
        for(Water w : waters){
            double dist = Point.getDistance(p,w.getPosition());
            if (foundW==null){
                foundW=w;
                foundDist=dist;
            } else {
                if (dist<foundDist){
                    foundW=w;
                    foundDist=dist;
                }
            }
        }
        return foundW;
    }

    public static World getInstance () {
        if (world == null)
            world = new World();
        return world;
    }
}