package entities;

import game.Game;
import game.World;
import utilities.Point;
import game.RegionManager;

import java.awt.*;

public abstract class Entity {
    RegionManager regionManager = RegionManager.getInstance();
    utilities.Point position;

    //registra in regionmanager una posizione random
    public Entity(){
        this(utilities.Point.getRandomPoint(Game.WIDTH, Game.HEIGHT));
    }

    public Entity(utilities.Point p){
        position=p;
        regionManager.register(this);
    }

    public abstract void update(double dt);
    public abstract void render(Graphics g);

    public Point getPosition() {
        return position;
    }
}