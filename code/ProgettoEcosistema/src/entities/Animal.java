package entities;

import utilities.Point;
import game.Parameters;
import game.Game;
import game.RegionManager;

import java.awt.*;

public abstract class Animal extends Entity {

    public enum State{NORMAL, MATE, HUNGER, DANGER,THIRSTY, DEAD};

    public static final int SIZE = 10;

    State state = State.NORMAL;
    utilities.Point destination = utilities.Point.getRandomPoint(Game.WIDTH, Game.HEIGHT);

    double age = 0;
    double food = 100;
    double water = 100;
    double sex = 0;
    double rad = 0;


    public Animal() {
        setDirection();
    }

    void updateBasicProperties(double dAge, double dFood, double dSex, double dWater) {
        age += dAge;
        food = Parameters.constrainParametersInRange(food + dFood, 0, Parameters.MAX_VALUE);
        sex = Parameters.constrainParametersInRange(sex + dSex, 0, Parameters.MAX_VALUE);
        water = Parameters.constrainParametersInRange(water + dWater, 0, Parameters.MAX_VALUE);
    }

    void setDirection() {
        double dy = destination.getY() - position.getY();
        double dx = destination.getX() - position.getX();
        rad = Math.atan2(dy, dx);
    }

    void setDirection(Point target) {
        double dy = target.getY() - position.getY();
        double dx = target.getX() - position.getX();
        rad = Math.atan2(dy, dx);
    }

    public void setState(State state) {
        switch (state) {
            case NORMAL:
                setNormalState();
                break;
            case HUNGER:
                setHungerState();
                break;
            case MATE:
                setMateState();
                break;
            case DEAD:
                setDeadState();
                break;
            case DANGER:
                setDangerState();
                break;
            case THIRSTY:
                setThirstyState();
                break;
            default:
                System.out.println("Invalid state, something went wrong");
        }
    }

    public State getState() {
        return state;
    }

    protected abstract void setNormalState();
    protected abstract void setMateState();
    protected abstract void setHungerState();
    protected abstract void setDangerState();
    protected abstract void setThirstyState();
    protected abstract void setDeadState();

    /**
     *
     * @param dt tempo trascorso in secondi
     */
    public void update(double dt) {
        regionManager.updateEntity(this);
    }
    public abstract void render(Graphics g);
}
