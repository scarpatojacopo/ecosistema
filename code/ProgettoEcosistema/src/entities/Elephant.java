package entities;

import game.World;
import utilities.Point;
import game.Parameters;
import game.Game;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class Elephant extends Animal {

    public static final int FILL_COLOR = 0x5AD2CC;
    public static boolean debugRender = false;

    double sightRange = Parameters.INIT_SIGHT_ELEPHANT;
    private double speed = Parameters.getRandomizedParameter(Parameters.INIT_SPEED_ELEPHANT, 0.1);
    private Elephant mateTarget = null;
    private Elephant elephantBaby = null;
    private Water source;

    public Elephant() {
        super();
    }

    public Elephant(Elephant p1, Elephant p2) {
        super();
        food = (p1.food + p2.food) / 2;
        position = utilities.Point.getRandomPointNear(p1.position, Parameters.NEW_BORN_DELIVER_RANGE);
        speed = Parameters.getRandomizedParameter((p1.speed + p2.speed)/2, Parameters.MUTATION_TOLERANCE_ELEPHANT);
        sightRange = Parameters.getRandomizedParameter((p1.sightRange + p2.sightRange)/2, Parameters.MUTATION_TOLERANCE_ELEPHANT);

        double initSpeed = Parameters.INIT_SPEED_ELEPHANT;
        double initSight = Parameters.INIT_SIGHT_ELEPHANT;
        double limit = Parameters.MAX_MUTATION_FACTOR;
        speed = Parameters.constrainParametersInRange(speed, initSpeed / limit, initSpeed * limit);
        sightRange = Parameters.constrainParametersInRange(sightRange, initSight / limit, initSight * limit);
    }

    @Override
    public void update(double dt) {
        super.update(dt);
        // Normal State
        if (state == State.NORMAL) {
            normalWalkAround(dt);
            // Change state:
            if (sex > Parameters.SEX_THRESHOLD) {
                setState(State.MATE);
            }
        }
        else if (state == State.THIRSTY) {
            if (source == null) {
                World world = World.getInstance();
                source = world.getNearestWater(position);
            } else {
                setDirection(source.position);
                double a = Math.exp((food - Parameters.MAX_VALUE) * Parameters.HUNGER_DECAY_EXP_FACTOR);
                position.translate(a * speed * Parameters.BOOST_FACTOR_ELEPHANT * Math.cos(rad) * dt, a * speed * Parameters.BOOST_FACTOR_ELEPHANT * Math.sin(rad) * dt);
                updateBasicProperties(+1 * dt, -Parameters.FOOD_DROP_RATE_ELEPHANT * Parameters.FOOD_DROP_BOOST_FACTOR * dt, +Parameters.SEX_INCREASE_RATE_ELEPHANT * dt, -Parameters.WATER_DROP_RATE_ELEPHANT * Parameters.WATER_DROP_BOOST_FACTOR * dt);
                if (utilities.Point.getDistance(position, source.position) <= source.getRadius())
                    drink();
            }
            // State changing
            if (water > Parameters.WATER_THRESHOLD)
                setState(State.NORMAL);
        }
            // Mate State
        else if (state == State.MATE ) {
            if (mateTarget != null && ((mateTarget.getState() == State.DEAD) || (utilities.Point.getDistance(position, mateTarget.position) > sightRange)))
                mateTarget = null;
            if (mateTarget == null) {
                ArrayList<Entity> entities = regionManager.getEntitiesInRange(this, sightRange);
                for (Entity e : entities) {
                    if ((e instanceof Elephant) && utilities.Point.getDistance(position, e.getPosition()) < sightRange && e != this) {
                        mateTarget = (Elephant) e;
                        break;
                    }
                }
                if (mateTarget == null)
                    normalWalkAround(dt);
            }
            else { //if (mateTarget != null)
                setDirection(mateTarget.position);
                double a = Math.exp((food - Parameters.MAX_VALUE) * Parameters.HUNGER_DECAY_EXP_FACTOR);
                position.translate(a * speed * Parameters.BOOST_FACTOR_ELEPHANT * Math.cos(rad) * dt, a * speed * Parameters.BOOST_FACTOR_ELEPHANT * Math.sin(rad) * dt);
                updateBasicProperties(+1 * dt, -Parameters.FOOD_DROP_RATE_ELEPHANT * Parameters.FOOD_DROP_BOOST_FACTOR * dt, +Parameters.SEX_INCREASE_RATE_ELEPHANT * dt, -Parameters.WATER_DROP_RATE_ELEPHANT * Parameters.WATER_DROP_BOOST_FACTOR * dt);
                if (utilities.Point.getDistance(position, mateTarget.position) < Parameters.COLLISION_RANGE)
                    mate();
            }
            if (sex < Parameters.SEX_THRESHOLD)
                setState(State.NORMAL);
        }

        if (food <= 0 || age >= Parameters.MAX_AGE_ELEPHANT)
            setState(State.DEAD);
        if (utilities.Point.isOutOfBoundary(position, 0, 0, Game.WIDTH, Game.HEIGHT)) {
            resetPosition();
            setState(State.NORMAL);
        }
        eat(dt);
    }

    @Override
    public void render(Graphics g) {
        if (state == State.NORMAL)
            g.setColor(new Color(0x101010));
        else if (state == State.HUNGER)
            g.setColor(new Color(0x202020));
        else if (state == State.MATE)
            g.setColor(new Color(0x2F2F2F));
        g.fillRoundRect(position.getX()-SIZE/2, position.getY()-SIZE/2, SIZE, SIZE, SIZE/2, SIZE/2);
    }

    public boolean isPregnant() {
        return (elephantBaby != null);
    }

    public void pregnant(Elephant e) {
        elephantBaby = e;
    }

    public Elephant deliveryBaby() {
        Elephant e = elephantBaby;
        elephantBaby = null;
        return e;
    }
    private void drink() {
        source = null;
        water += Parameters.WATER_DRINK_VALUE_ELEPHANT;
        water = (water > Parameters.MAX_VALUE) ? Parameters.MAX_VALUE : water;
    }
    private void normalWalkAround(double dt) {
        //From normal
        if (utilities.Point.getDistance(position, destination) < Parameters.COLLISION_RANGE) {
            destination = utilities.Point.getRandomPoint(Game.WIDTH, Game.HEIGHT);
        }
        setDirection();
        double a = Math.exp((food - Parameters.MAX_VALUE) * Parameters.HUNGER_DECAY_EXP_FACTOR);
        position.translate(a * speed * Math.cos(rad) * dt, a * speed * Math.sin(rad) * dt);
        updateBasicProperties(+1 * dt, -Parameters.FOOD_DROP_RATE_ELEPHANT * dt, +Parameters.SEX_INCREASE_RATE_ELEPHANT * dt, -Parameters.WATER_DROP_RATE_ELEPHANT * dt);
    }

    private void mate() {
        this.sex = 0;
        mateTarget.sex = 0;
        double a = Math.exp((food + mateTarget.food - 2 * Parameters.MAX_VALUE)/2 * Parameters.PREGNANT_EXP_DECAY_FACTOR_ELEPHANT);
        if ((new Random().nextFloat() < Parameters.PREGNANT_PROBABILITY_ELEPHANT) && elephantBaby == null) {
            if (!mateTarget.isPregnant())
                mateTarget.pregnant(new Elephant(this, mateTarget));
        }
        mateTarget = null;
    }

    private void eat(double dt) {
        int elephantNum = 0;
        ArrayList<Entity> entities = regionManager.getRegion(position);
        for(Entity e: entities)
            elephantNum += (e instanceof Elephant) ? 1 : 0;
        elephantNum = (int) Parameters.constrainParametersInRange((double)elephantNum - Parameters.FOOD_SHORTAGE_TH_ELEPHANT, 0, Integer.MAX_VALUE);
        food += Parameters.FOOD_EAT_RATE_ELEPHANT * Math.exp(-elephantNum * Parameters.FOOD_SHORTAGE_EXP_ELEPHANT) * dt;
        food = Parameters.constrainParametersInRange(food, Integer.MIN_VALUE, Parameters.MAX_VALUE);
    }

    private void resetPosition() {
        double x = position.getX();
        x = x >= Game.WIDTH ? x - Game.WIDTH : x;
        x = x < 0 ? x + Game.WIDTH : x;
        double y = position.getY();
        y = y >= Game.HEIGHT ? y - Game.HEIGHT : y;
        y = y < 0 ? y + Game.HEIGHT : y;
        position = new Point(x, y);
    }

    public double getSightRange() {
        return sightRange;
    }

    public double getSpeed() {
        return speed;
    }

    @Override
    protected void setNormalState() {
        mateTarget = null;
        state = State.NORMAL;
    }

    @Override
    protected void setMateState() {
        state = State.MATE;
    }

    @Override
    protected void setHungerState() {
        mateTarget = null;
        state = State.HUNGER;
    }
    @Override
    protected void setThirstyState(){ state=State.THIRSTY; }
    @Override
    protected void setDangerState() {
        mateTarget = null;
        state = State.DANGER;
    }

    @Override
    protected void setDeadState() {
        state = State.DEAD;
    }
}