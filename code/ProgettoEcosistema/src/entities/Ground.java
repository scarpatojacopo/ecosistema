package entities;

import java.awt.*;
import game.Game;
import utilities.Point;

public abstract class Ground {
    utilities.Point position = utilities.Point.getRandomPoint(Game.WIDTH, Game.HEIGHT);

    public abstract void update(double dt);
    public abstract void render(Graphics g);

    public Point getPosition() {
        return position;
    }
}
