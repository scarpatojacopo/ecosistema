package entities;

import game.World;
import utilities.Point;
import game.Parameters;
import game.Game;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class Gazelle extends Animal {

    double sightRange = Parameters.INIT_SIGHT_GAZELLE;
    private double speed = Parameters.getRandomizedParameter(Parameters.INIT_SPEED_GAZELLE, 0.1);
    private Gazelle mateTarget = null;
    private Lion dangerSource = null;
    private Gazelle gazelleBaby = null;
    private Water source;

    public Gazelle() {
        super();
    }

    public Gazelle(Gazelle p1, Gazelle p2) {
        super();
        food = (p1.food + p2.food) / 2;
        position = utilities.Point.getRandomPointNear(p1.position, Parameters.NEW_BORN_DELIVER_RANGE);
        speed = Parameters.getRandomizedParameter((p1.speed + p2.speed)/2, Parameters.MUTATION_TOLERANCE_GAZELLE);
        sightRange = Parameters.getRandomizedParameter((p1.sightRange + p2.sightRange)/2, Parameters.MUTATION_TOLERANCE_GAZELLE);

        double initSpeed = Parameters.INIT_SPEED_GAZELLE;
        double initSight = Parameters.INIT_SIGHT_GAZELLE;
        double limit = Parameters.MAX_MUTATION_FACTOR;
        speed = Parameters.constrainParametersInRange(speed, initSpeed / limit, initSpeed * limit);
        sightRange = Parameters.constrainParametersInRange(sightRange, initSight / limit, initSight * limit);
    }

    @Override
    public void update(double dt) {
        super.update(dt);
        // Normal State:
        if (state == State.NORMAL) {
            normalWalkAround(dt);
            // Change state:
            if (dangerSource == null)
                dangerSource = alertDangerSource();
            if (dangerSource != null)
                setState(State.DANGER);
            else if (sex > Parameters.SEX_THRESHOLD) {
                setState(State.MATE);
            }
        }
        // Mate State
        else if (state == State.MATE ) {
            if (mateTarget != null && ((mateTarget.getState() == State.DEAD) || (utilities.Point.getDistance(position, mateTarget.position) > sightRange)))
                mateTarget = null;
            if (mateTarget == null) {
                ArrayList<Entity> entities = regionManager.getEntitiesInRange(this, sightRange);
                for (Entity e : entities) {
                    if ((e instanceof Gazelle) && utilities.Point.getDistance(position, e.getPosition()) < sightRange && e != this) {
                        mateTarget = (Gazelle) e;
                        break;
                    }
                }
                if (mateTarget == null)
                    normalWalkAround(dt);
            }
            else { //if (mateTarget != null)
                setDirection(mateTarget.position);
                double a = Math.exp((food - Parameters.MAX_VALUE) * Parameters.HUNGER_DECAY_EXP_FACTOR);
                position.translate(a * speed * Parameters.BOOST_FACTOR_GAZELLE * Math.cos(rad) * dt, a * speed * Parameters.BOOST_FACTOR_GAZELLE * Math.sin(rad) * dt);
                updateBasicProperties(+1 * dt, -Parameters.FOOD_DROP_RATE_GAZELLE * Parameters.FOOD_DROP_BOOST_FACTOR * dt, +Parameters.SEX_INCREASE_RATE_GAZELLE * dt, -Parameters.WATER_DROP_RATE_GAZELLE * Parameters.WATER_DROP_BOOST_FACTOR * dt);
                if (utilities.Point.getDistance(position, mateTarget.position) < Parameters.COLLISION_RANGE)
                    mate();
            }

            if (dangerSource == null)
                dangerSource = alertDangerSource();

            if (dangerSource != null)
                setState(State.DANGER);
            else if (sex < Parameters.SEX_THRESHOLD)
                setState(State.NORMAL);
        }
        //Thirsty State
        else if (state == State.THIRSTY) {
            if (source == null) {
                World world = World.getInstance();
                source = world.getNearestWater(position);
            } else {
                setDirection(source.position);
                double a = Math.exp((food - Parameters.MAX_VALUE) * Parameters.HUNGER_DECAY_EXP_FACTOR);
                position.translate(a * speed * Parameters.BOOST_FACTOR_GAZELLE * Math.cos(rad) * dt, a * speed * Parameters.BOOST_FACTOR_GAZELLE * Math.sin(rad) * dt);
                updateBasicProperties(+1 * dt, -Parameters.FOOD_DROP_RATE_GAZELLE * Parameters.FOOD_DROP_BOOST_FACTOR * dt, +Parameters.SEX_INCREASE_RATE_GAZELLE * dt, -Parameters.WATER_DROP_RATE_GAZELLE * Parameters.WATER_DROP_BOOST_FACTOR * dt);
                if (utilities.Point.getDistance(position, source.position) <= source.getRadius())
                    drink();
            }
            // State changing
            if (water > Parameters.WATER_THRESHOLD)
                setState(State.NORMAL);
        }
        // Danger State
        else if (state == State.DANGER) {
            if (dangerSource != null && dangerSource.getState() == State.DEAD)
                dangerSource = null;
            if (dangerSource == null)
                normalWalkAround(dt);
            else {
                setDirection(dangerSource.position);
                rad += Math.PI;
                double a = Math.exp((food - Parameters.MAX_VALUE) * Parameters.HUNGER_DECAY_EXP_FACTOR);
                position.translate(a * speed * Parameters.BOOST_FACTOR_GAZELLE * Math.cos(rad) * dt, a * speed * Parameters.BOOST_FACTOR_GAZELLE * Math.sin(rad) * dt);
                updateBasicProperties(+1 * dt, -Parameters.FOOD_DROP_RATE_GAZELLE * Parameters.FOOD_DROP_BOOST_FACTOR * dt, +Parameters.SEX_INCREASE_RATE_GAZELLE * dt, -Parameters.WATER_DROP_RATE_GAZELLE * Parameters.WATER_DROP_BOOST_FACTOR * dt);
            }
            // Change State
            if (dangerSource != null && utilities.Point.getDistance(dangerSource.position, position) < sightRange) { }
            else {
                dangerSource = alertDangerSource();
                if (dangerSource == null && sex < Parameters.SEX_THRESHOLD)
                    setState(State.NORMAL);
                else if (dangerSource == null && sex > Parameters.SEX_THRESHOLD)
                    setState(State.MATE);
            }
        }

        if (food <= 0 || age >= Parameters.MAX_AGE_GAZELLE)
            setState(State.DEAD);
        if (utilities.Point.isOutOfBoundary(position, 0, 0, Game.WIDTH, Game.HEIGHT)) {
            resetPosition();
            setState(State.NORMAL);
        }
        eat(dt);
    }

    @Override
    public void render(Graphics g) {
        if (state == State.NORMAL)
            g.setColor(new Color(0x836038));
        else if (state == State.DANGER)
            g.setColor(new Color(0x906D45));
        else if (state == State.MATE)
            g.setColor(new Color(0x9C7C53));
        g.fillRoundRect(position.getX()-SIZE/2, position.getY()-SIZE/2, SIZE, SIZE, SIZE/2, SIZE/2);
    }

    public boolean isPregnant() {
        return (gazelleBaby != null);
    }

    public void pregnant(Gazelle g) {
        gazelleBaby = g;
    }

    public Gazelle deliveryBaby() {
        Gazelle g = gazelleBaby;
        gazelleBaby = null;
        return g;
    }
    private void drink() {
        source = null;
        water += Parameters.WATER_DRINK_VALUE_GAZELLE;
        water = (water > Parameters.MAX_VALUE) ? Parameters.MAX_VALUE : water;
    }
    private Lion alertDangerSource() {
        ArrayList<Entity> entities = regionManager.getEntitiesInRange(this, sightRange);
        for(Entity e : entities) {
            if ((e instanceof Lion) && utilities.Point.getDistance(e.position, position) < sightRange) {
                return (Lion) e;
            }
        }
        return null;
    }

    private void normalWalkAround(double dt) {
        //From normal
        if (utilities.Point.getDistance(position, destination) < Parameters.COLLISION_RANGE) {
            destination = utilities.Point.getRandomPoint(Game.WIDTH, Game.HEIGHT);
        }
        setDirection();
        double a = Math.exp((food - Parameters.MAX_VALUE) * Parameters.HUNGER_DECAY_EXP_FACTOR);
        position.translate(a * speed * Math.cos(rad) * dt, a * speed * Math.sin(rad) * dt);
        updateBasicProperties(+1 * dt, -Parameters.FOOD_DROP_RATE_GAZELLE * dt, +Parameters.SEX_INCREASE_RATE_GAZELLE * dt, -Parameters.WATER_DROP_RATE_GAZELLE * dt);
    }

    private void mate() {
        this.sex = 0;
        mateTarget.sex = 0;
        if ((new Random().nextFloat() < Parameters.PREGNANT_PROBABILITY_GAZELLE) && gazelleBaby == null) {
            if (!mateTarget.isPregnant())
                mateTarget.pregnant(new Gazelle(this, mateTarget));
        }
        mateTarget = null;
    }

    private void eat(double dt) {
        int gazelleNum = 0;
        ArrayList<Entity> entities = regionManager.getRegion(position);
        for(Entity e: entities)
            gazelleNum += (e instanceof Gazelle) ? 1 : 0;
        int foodShortage = (int) Parameters.constrainParametersInRange((double)gazelleNum - Parameters.FOOD_SHORTAGE_TH_GAZELLE, 0, Integer.MAX_VALUE);
        food += Parameters.FOOD_EAT_RATE_GAZELLE * Math.exp(-foodShortage * Parameters.FOOD_SHORTAGE_EXP_GAZELLE) * dt;
        food = Parameters.constrainParametersInRange(food, Integer.MIN_VALUE, Parameters.MAX_VALUE);
    }

    private void resetPosition() {
        double x = position.getX();
        x = x >= Game.WIDTH ? x - Game.WIDTH : x;
        x = x < 0 ? x + Game.WIDTH : x;
        double y = position.getY();
        y = y >= Game.HEIGHT ? y - Game.HEIGHT : y;
        y = y < 0 ? y + Game.HEIGHT : y;
        position = new Point(x, y);
    }

    public double getSightRange() {
        return sightRange;
    }

    public double getSpeed() {
        return speed;
    }

    @Override
    protected void setNormalState() {
        mateTarget = null;
        dangerSource = null;
        state = State.NORMAL;
    }

    @Override
    protected void setMateState() {
        state = State.MATE;
    }

    @Override
    protected void setHungerState() { state = State.HUNGER; }

    @Override
    protected void setDangerState() {
        mateTarget = null;
        state = State.DANGER;
    }

    @Override
    protected void setThirstyState(){ state = State.THIRSTY; }

    @Override
    protected void setDeadState() {
        state = State.DEAD;
    }
}