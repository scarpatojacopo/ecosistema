package entities;

import game.World;
import utilities.Point;
import game.Parameters;
import game.Game;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;


public class Lion extends Animal {

    public static final int FILL_COLOR = 0xD44B4C;

    private boolean debugRender = false;
    private double speed = Parameters.getRandomizedParameter(Parameters.INIT_SPEED_LION, 0.1);
    private double sightRange = Parameters.INIT_SIGHT_LION;
    private Gazelle huntTarget = null;
    private Lion mateTarget = null;
    private Lion lionBaby = null;
    private Water source = null;

    public Lion() {
        super();
    }

    public Lion(Lion p1, Lion p2) {
        super();
        food = (p1.food + p2.food) / 2;
        position = utilities.Point.getRandomPointNear(p1.position, Parameters.NEW_BORN_DELIVER_RANGE);
        speed = Parameters.getRandomizedParameter((p1.speed + p2.speed)/2, Parameters.MUTATION_TOLERANCE_LION);
        sightRange = Parameters.getRandomizedParameter((p1.sightRange + p2.sightRange)/2, Parameters.MUTATION_TOLERANCE_LION);

        double initSpeed = Parameters.INIT_SPEED_LION;
        double initSight = Parameters.INIT_SIGHT_LION;
        double limit = Parameters.MAX_MUTATION_FACTOR;
        speed = Parameters.constrainParametersInRange(speed, initSpeed / limit, initSpeed * limit);
        sightRange = Parameters.constrainParametersInRange(sightRange, initSight / limit, initSight * limit);
    }

    @Override
    public void update(double dt) {
        super.update(dt);
        // Normal State
        if(state == State.NORMAL){
            normalWalkAround(dt);
            // State changing
            if (food < Parameters.FOOD_THRESHOLD)
                setState(State.HUNGER);
            else if (food > Parameters.FOOD_THRESHOLD && sex > Parameters.SEX_THRESHOLD)
                setState(State.MATE);
        }
        // Hunger State
        else if (state == State.HUNGER) {
            // Hunger Behavior
            if (huntTarget != null && huntTarget.getState() == State.DEAD) {
                huntTarget = null;
            }
            if (huntTarget == null) {
                ArrayList<Entity> entities = regionManager.getEntitiesInRange(this, sightRange);
                for (Entity e : entities) {
                    if ((e instanceof Gazelle) && utilities.Point.getDistance(e.getPosition(), position) <= sightRange) {
                        huntTarget = (Gazelle) e;
                        break;
                    }
                }
                if (huntTarget == null)
                    normalWalkAround(dt);
            } else {
                setDirection(huntTarget.getPosition());
                double a = Math.exp((food - Parameters.MAX_VALUE) * Parameters.HUNGER_DECAY_EXP_FACTOR);
                position.translate(a * Parameters.BOOST_FACTOR_LION * speed * Math.cos(rad) * dt, a * Parameters.BOOST_FACTOR_LION * speed * Math.sin(rad) * dt);
                updateBasicProperties(+1 * dt, -Parameters.FOOD_DROP_RATE_LION * Parameters.FOOD_DROP_BOOST_FACTOR * dt, Parameters.SEX_INCREASE_RATE_LION * dt, -Parameters.WATER_DROP_RATE_LION * Parameters.WATER_DROP_BOOST_FACTOR * dt);
                if (utilities.Point.getDistance(position, huntTarget.position) <= Parameters.COLLISION_RANGE) {
                    hunt();
                }
            }
            // State changing
            if (food > Parameters.FOOD_THRESHOLD && sex < Parameters.SEX_THRESHOLD)
                setState(State.NORMAL);
            else if (food > Parameters.FOOD_THRESHOLD && sex >= Parameters.SEX_THRESHOLD)
                setState(State.MATE);

        }
        //Thirsty State
        else if (state == State.THIRSTY) {
            if (source == null) {
                World world = World.getInstance();
                source = world.getNearestWater(position);
            } else {
                setDirection(source.position);
                double a = Math.exp((food - Parameters.MAX_VALUE) * Parameters.HUNGER_DECAY_EXP_FACTOR);
                position.translate(a * Parameters.BOOST_FACTOR_LION * speed * Math.cos(rad) * dt, a * Parameters.BOOST_FACTOR_LION * speed * Math.sin(rad) * dt);
                updateBasicProperties(+1 * dt, -Parameters.FOOD_DROP_RATE_LION * Parameters.FOOD_DROP_BOOST_FACTOR * dt, +Parameters.SEX_INCREASE_RATE_LION * dt, -Parameters.WATER_DROP_RATE_LION * Parameters.WATER_DROP_BOOST_FACTOR * dt);
                if (utilities.Point.getDistance(position, source.position) <= source.getRadius())
                    drink();
            }
            // State changing
            if (water > Parameters.WATER_THRESHOLD)
                setState(State.NORMAL);
        }
        // Mate State
        else if (state == State.MATE) {
            if (mateTarget != null && ((mateTarget.getState() == State.DEAD) || (utilities.Point.getDistance(position, mateTarget.position) > sightRange)))
                mateTarget = null;
            if (mateTarget == null) {
                ArrayList<Entity> entities = regionManager.getRegion(position);
                for(Entity e : entities) {
                    if ((e instanceof Lion) && utilities.Point.getDistance(position, e.getPosition()) < sightRange && e != this) {
                        mateTarget = (Lion) e;
                        break;
                    }
                }
                if (mateTarget == null)
                    normalWalkAround(dt);
            } else {
                setDirection(mateTarget.position);
                double a = Math.exp((food - Parameters.MAX_VALUE) * Parameters.HUNGER_DECAY_EXP_FACTOR);
                position.translate(a * Parameters.BOOST_FACTOR_LION * speed * Math.cos(rad) * dt, a * Parameters.BOOST_FACTOR_LION * speed * Math.sin(rad) * dt);
                updateBasicProperties(+1 * dt, -Parameters.FOOD_DROP_RATE_LION * Parameters.FOOD_DROP_BOOST_FACTOR * dt, +Parameters.SEX_INCREASE_RATE_LION * dt, -Parameters.WATER_DROP_RATE_LION * Parameters.WATER_DROP_BOOST_FACTOR * dt);
                if (utilities.Point.getDistance(position, mateTarget.position) <= Parameters.COLLISION_RANGE)
                    mate();
            }
            // State changing
            if (food < Parameters.FOOD_SHORTAGE_TH_LION)
                setState(State.HUNGER);
            else if (water < Parameters.WATER_SHORTAGE_TH_LION)
                setState(State.THIRSTY);
            else if (food > Parameters.FOOD_SHORTAGE_TH_LION && sex < Parameters.SEX_THRESHOLD)
                setState(State.NORMAL);
        }
        if (food <= 0 || water<=0 || age >= Parameters.MAX_AGE_LION )
            setState(State.DEAD);
    }

    @Override
    public void render(Graphics g) {
        if (state == State.NORMAL)
            g.setColor(new Color(0xB01002));
        else if (state == State.HUNGER)
            g.setColor(new Color(0xA10C07));
        else if (state == State.MATE)
            g.setColor(new Color(0x800F02));
        g.fillRoundRect(position.getX()-SIZE/2, position.getY()-SIZE/2, SIZE, SIZE, SIZE/2, SIZE/2);

        if(debugRender) {
            g.drawString("food:" + Integer.toString((int) food), position.getX() - 3, position.getY() - 7);
            g.drawString("age:" + Integer.toString((int) age), position.getX() - 3, position.getY() - 16);
            g.drawString("state:" + state, position.getX() - 3, position.getY() - 25);
            g.drawString("water:" + Integer.toString((int) water), position.getX() - 3, position.getY() - 34);
        }
    }

    private void normalWalkAround(double dt) {
        if (utilities.Point.getDistance(position, destination) < Parameters.COLLISION_RANGE) {
            destination = Point.getRandomPoint(Game.WIDTH, Game.HEIGHT);
        }
        setDirection();
        double a = Math.exp((food - Parameters.MAX_VALUE) * Parameters.HUNGER_DECAY_EXP_FACTOR);
        position.translate(a * speed * Math.cos(rad) * dt, a * speed * Math.sin(rad) * dt);
        updateBasicProperties(+1 * dt, -Parameters.FOOD_DROP_RATE_LION * dt, Parameters.SEX_INCREASE_RATE_LION * dt, -Parameters.WATER_DROP_RATE_LION * dt);
    }

    private void hunt() {
        huntTarget.setState(State.DEAD);
        huntTarget = null;
        food += Parameters.FOOD_EAT_VALUE_LION;
        food = (food > Parameters.MAX_VALUE) ? 100 : food;
    }

    private void mate() {
        this.sex = 0;
        mateTarget.sex = 0;
        double a = Math.exp((food + mateTarget.food - 2 * Parameters.MAX_VALUE)/2 * Parameters.PREGNANT_EXP_DECAY_FACTOR_LION);
        if ((new Random().nextFloat() < Parameters.PREGNANT_PROBABILITY_LION * a) && lionBaby == null) {
            if (!mateTarget.isPregnant())
                mateTarget.pregnant(new Lion(this, mateTarget));
        }
        food -= Parameters.FOOD_DROP_SEX_LION;
        mateTarget = null;
    }
    private void drink() {
        source = null;
        water += Parameters.WATER_DRINK_VALUE_LION;
        water = (water > Parameters.MAX_VALUE) ? Parameters.MAX_VALUE : water;
    }

    public boolean isPregnant() {
        return lionBaby != null;
    }

    public void pregnant(Lion w) { lionBaby = w; }

    public Lion deliveryBaby() {
        Lion l1 = lionBaby;
        lionBaby = null;
        return l1;
    }

    public double getSightRange() {
        return sightRange;
    }

    public double getSpeed() {
        return speed;
    }

    @Override
    protected void setNormalState() {
        huntTarget = null;
        mateTarget = null;
        state = State.NORMAL;
    }

    @Override
    protected void setMateState() {
        huntTarget = null;
        state = State.MATE;
    }

    @Override
    protected void setHungerState() {
        mateTarget = null;
        state = State.HUNGER;
    }
    @Override
    protected void setThirstyState(){ state=State.THIRSTY; }

    @Override
    protected void setDangerState() {
        System.out.println("Error: Lion state will not be dangerous.");
    }

    @Override
    protected void setDeadState() {
        state = State.DEAD;
    }
}