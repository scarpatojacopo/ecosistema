package entities;


import utilities.Point;

import java.awt.*;

public class Water extends Entity {
    int rad;

    public Water(Point p, int radius){
        super(p);
        rad=radius;
    }

    @Override
    public void update(double dt) {

    }
    public double getRadius(){
        return rad;
    }


    public void render(Graphics g) {
        g.setColor(new Color(0x80007FFF, true));
        g.fillOval(position.getX()-rad, position.getY()-rad, rad, rad);
    }
}
