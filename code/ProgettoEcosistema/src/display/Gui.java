package display;

import game.Game;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

public class Gui {

    public static final Color GUI_BGCOLOR = new Color(0xF7E89F);

    private JFrame frame = new JFrame();
    private JPanel panel = new JPanel();
    private JPanel ctrlPanel = new JPanel();
    private JButton startButton = new JButton("Plotter");
    private Canvas canvas = new Canvas();

    public Gui(){
        canvas.setSize(new Dimension(Game.WIDTH, Game.HEIGHT));
        frame.add(panel);
        frame.setTitle("Savana Ecosystem");
        panel.setLayout(new BorderLayout());
        panel.setBorder(new EmptyBorder(20, 15, 10, 15));
        ctrlPanel.setBorder(new EmptyBorder(5, 0, 0, 0));
        ctrlPanel.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        panel.add(canvas, BorderLayout.CENTER);
        panel.add(ctrlPanel, BorderLayout.SOUTH);

        canvas.setBackground(GUI_BGCOLOR);
        panel.setBackground(GUI_BGCOLOR);
        ctrlPanel.setBackground(GUI_BGCOLOR);

        frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public Canvas getCanvas(){
        return canvas;
    }

    // Event Handlers
    public void setStartButtonAction(ActionListener handler) {
        startButton.addActionListener(handler);
    }

    static public Font getDebugFont() {
        return new Font("Arial", Font.PLAIN, 8);
    }
}
